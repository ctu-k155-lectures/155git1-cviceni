#!/usr/bin/env python3

def dms2float(value):
    dms = value.split(':')
    return int(dms[0]) + (int(dms[1]) / 60) + (float(dms[2]) / 3600)

out = []
inpf = open('kina.txt')
for line in inpf.read().splitlines():
    # items = line.split(line)
    id, lat, lon, name = line.split(',')
    print (id, lon, lat)
    # dms = lon.split(':')
    # x = int(dms[0]) + (int(dms[1]) / 60) + (float(dms[2]) / 3600)
    print (id, dms2float(lon), dms2float(lat))
    out.append([id, dms2float(lon), dms2float(lat), name])
inpf.close()

outf = open('kina-out.txt', 'w')
for id, lon, lat, name in out:
    outf.write('{},{:.6f},{:.6f},{}\n'.format(id, lon, lat, name))
outf.close()
